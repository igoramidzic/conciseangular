﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConciseAngular.Data.Context;
using ConciseAngular.Data.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ConciseAngular.Controllers
{
    [Route("api/[controller]")]
    public class TodosController : Controller
    {
        private readonly ConciseContext _context;

        public TodosController(ConciseContext context)
        {
            _context = context;
        }

        // GET api/todos: Get all todos
        [HttpGet]
        public ActionResult<List<TodoItem>> Get()
        {
            List<TodoItem> todos = _context.TodoItems.ToList();
            return Ok(todos);
        }

        // POST api/todos: Create one todo
        [HttpPost]
        public ActionResult<TodoItem> Post([FromBody] TodoItem todo)
        {
            TodoItem NewTodoItem = new TodoItem
            {
                Name = todo.Name,
                Completed = false
            };

            _context.TodoItems.Add(NewTodoItem);
            _context.SaveChanges();

            return Ok(NewTodoItem);
        }

        // PUT api/todos/{id}: Update one todo
        [HttpPut("{id}")]
        public ActionResult<TodoItem> Put([FromBody] TodoItem todo, long Id)
        {
            TodoItem UpdateTodoItem = _context.TodoItems.FirstOrDefault(t => t.Id == Id);

            if (UpdateTodoItem == null)
            {
                return NotFound(new { error = "Todo item does not exist" });
            }

            UpdateTodoItem.Name = todo.Name;
            UpdateTodoItem.Completed = todo.Completed;

            _context.TodoItems.Update(UpdateTodoItem);
            _context.SaveChanges();

            return UpdateTodoItem;
        }

        // DELETE api/todos/{id}: Delete one todo
        [HttpDelete("{id}")]
        public ActionResult<TodoItem> Delete(long Id)
        {
            TodoItem DeleteTodoItem = _context.TodoItems.FirstOrDefault(t => t.Id == Id);

            if (DeleteTodoItem == null)
            {
                return NotFound(new { error = "Todo item does not exist" });
            }

            _context.TodoItems.Remove(DeleteTodoItem);
            _context.SaveChanges();

            return Ok(DeleteTodoItem);
        }

        // DELETE api/todos: Delete all todos
        [HttpDelete]
        public ActionResult<List<TodoItem>> Delete()
        {
            List<TodoItem> ReturnList = _context.TodoItems.ToList();
            List<TodoItem> DeleteList = _context.TodoItems.ToList();

            _context.TodoItems.RemoveRange(DeleteList);
            _context.SaveChanges();

            return ReturnList;
        }
    }
}
