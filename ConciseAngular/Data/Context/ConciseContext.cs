﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Linq;
using ConciseAngular.Data.Models;

namespace ConciseAngular.Data.Context
{
    public partial class ConciseContext : IdentityDbContext
    {
        public ConciseContext(DbContextOptions<ConciseContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }
    }
}
