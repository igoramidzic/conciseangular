﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConciseAngular.Data.Models
{
    public class TodoItem
    {
        [Required]
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Completed { get; set; }
    }
}
