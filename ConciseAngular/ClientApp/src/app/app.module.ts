import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TodosComponent } from './pages/todos/todos.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { TodosListComponent } from './components/todos/todos-list/todos-list.component';
import { CreateTodoComponent } from './components/todos/create-todo/create-todo.component';
import { EditTodoComponent } from './components/todos/edit-todo/edit-todo.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    TodosComponent,
    TodosListComponent,
    CreateTodoComponent,
    EditTodoComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/todos', pathMatch: 'full' },
      { path: 'todos', component: TodosComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
