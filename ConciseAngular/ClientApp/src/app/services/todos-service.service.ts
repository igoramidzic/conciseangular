import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TodoItem } from '../models/todo-item';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class TodosService {
  public todosList: BehaviorSubject<TodoItem[]> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
    this.updateTodosList();
  }

  // Get all todos: GET
  getTodos(): Observable<any> {
    return this.http.get(environment.apiBase + "todos")
  }

  // Create a todo: POST
  createTodo(todo: TodoItem): Observable<any> {
    var options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.post(environment.apiBase + "todos", todo, options);
  }

  updateTodo(todo: TodoItem): Observable<any> {
    var options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.put(environment.apiBase + "todos/" + todo.id, todo, options);
  }

  deleteTodo(todo: TodoItem): Observable<any> {
    var options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.delete(environment.apiBase + "todos/" + todo.id);
  }

  updateTodosList(): void {
    this.getTodos().subscribe((res: TodoItem[]) => {
      this.todosList.next(res);
    })
  }

}
