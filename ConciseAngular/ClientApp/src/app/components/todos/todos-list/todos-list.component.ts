import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodoItem } from '../../../models/todo-item';
import { TodosService } from '../../../services/todos-service.service';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.css']
})
export class TodosListComponent implements OnInit {

  public todosList: TodoItem[];
  @Input() showCreateForm: boolean;
  @Output() toggledCreateForm = new EventEmitter<boolean>();
  @Output() toggledEditForm = new EventEmitter<TodoItem>();

  deletedItems: TodoItem[] = [];

  constructor(private todosService: TodosService) {
  }

  ngOnInit() {
    this.todosService.todosList
      .subscribe(res => {
        this.todosList = res;
        console.log(res)
      })
  }

  toggleCreateForm(): void {
    this.showCreateForm = true;
    this.toggledCreateForm.emit(true);
  }

  toggleEditForm(todo: TodoItem): void {
    var editTodo = <TodoItem>{
      id: todo.id,
      name: todo.name,
      completed: todo.completed
    }
    this.toggledEditForm.emit(editTodo);
  }

  updateCompleted(todo: TodoItem): void {
    todo.completed = !todo.completed;
    this.todosService.updateTodo(todo)
      .subscribe(res => {
        this.todosService.updateTodosList();
        console.log(res);
      })
  }

  deleteTodo(todo: TodoItem): void {
    this.todosService.deleteTodo(todo)
      .subscribe(res => {
        console.log(res);
        this.deletedItems.push(todo);
        setTimeout(() => {
          this.timeoutUndoDeleteTodo(todo);
        }, 5000)
      })
  }

  undoDeleteTodo(todo: TodoItem): void {
    var restoreTodo = <TodoItem>{
      name: todo.name,
      completed: todo.completed
    }

    this.todosService.createTodo(restoreTodo)
      .subscribe(res => {
        this.removeTodoFromDeleted(todo);
        this.todosService.updateTodosList();
      });
  }

  timeoutUndoDeleteTodo(todo: TodoItem): void {
    if (this.checkIfTodoIsBeingDeleted(todo)) {
      this.removeTodoFromDeleted(todo);
      this.todosService.updateTodosList();
    }
  }

  checkIfTodoIsBeingDeleted(todo: TodoItem): boolean {
    for (let i = 0; i < this.deletedItems.length; i++) {
      if (this.deletedItems[i].id == todo.id) {
        return true;
      }
    }
    return false;
  }

  removeTodoFromDeleted(todo: TodoItem): void {
    for (let i = 0; i < this.deletedItems.length; i++) {
      if (this.deletedItems[i].id == todo.id) {
        this.deletedItems.splice(i, 1);
        return;
      }
    }
  }

}
