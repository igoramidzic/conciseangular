import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodoItem } from '../../../models/todo-item';
import { TodosService } from '../../../services/todos-service.service';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.css']
})
export class EditTodoComponent implements OnInit {

  @Input() editTodo: TodoItem;
  validation: string;
  @Output() hideEditForm = new EventEmitter<boolean>();

  constructor(private todosService: TodosService) { }

  ngOnInit() {
  }

  validateForm(): boolean {
    if (this.editTodo.name == "" || this.editTodo.name == null) {
      this.validation = "Name cannot be empty";
      return false;
    }
    return true;
  }

  onEditTodo(): void {

    if (!this.validateForm()) {
      return;
    }

    this.todosService.updateTodo(this.editTodo)
      .subscribe(res => {
        this.todosService.updateTodosList();
        this.toggleEditForm();
      }, error => {
        this.hideEditForm.emit(true);
      });
  }

  toggleEditForm(): void {
    this.hideEditForm.emit(true);
  }

}
