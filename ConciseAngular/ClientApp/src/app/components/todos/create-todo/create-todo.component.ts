import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../../../services/todos-service.service';
import { TodoItem } from '../../../models/todo-item';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  @Output() toggledCreateForm = new EventEmitter<boolean>();

  todo: TodoItem;
  validation: string;

  constructor(private todosService: TodosService) { }

  ngOnInit() {
    this.todo = <TodoItem>{
      name: "",
      completed: false
    }
  }

  validateForm(): boolean {
    if (this.todo.name == "" || this.todo.name == null) {
      this.validation = "Name cannot be empty";
      return false;
    }
    return true;
  }

  onCreateTodo(): void {

    if (!this.validateForm()) {
      return;
    }

    this.todosService.createTodo(this.todo)
      .subscribe(res => {
        this.todosService.updateTodosList();
        this.toggleCreateForm();
      });
  }

  toggleCreateForm(): void {
    this.toggledCreateForm.emit(false);
  }

}
