import { Component, OnInit } from '@angular/core';
import { TodosService } from '../../services/todos-service.service';
import { TodoItem } from '../../models/todo-item';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  providers: [TodosService]
})
export class TodosComponent implements OnInit {

  showCreateForm: boolean = false;
  showEditForm: boolean = false;

  editTodo: TodoItem;

  constructor() { }

  ngOnInit() {
  }

  onToggleCreateForm(status): void {
    this.showEditForm = false;
    this.showCreateForm = status;
  }

  onShowEditForm(todo): void {
    this.showCreateForm = false;
    this.showEditForm = true;
    this.editTodo = todo;
  }

  onHideEditForm(): void {
    this.showEditForm = false;
  }

}
